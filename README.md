# CyberCriminology 
Application of Routine Activity Theory into Cyber Security: 

This project is to create a collaborative approach by combining Criminology and CyberSecurity to offer a solution to address cyber-crimes. The focus of this research idea is to reduce to cyber-attacks by combining criminological approaches with technology. 
In a cyber-attack victims and perpetrators may never physically meet face to face or encounter one another; however, the damage can be just as life changing as traditional crime. Cybercriminals harm their victims virtually through the help of Internet, thus preserving their anonymity. 
The goal to use intervention strategy is to determine how the distribution and utilization of information security knowledge can be used to reduce cybercrime victimization. This particular type of research is helpful for several institutions including schools, universities, small businesses, and automobile industries, etc. 
As cybercriminals grow in number and sophistication, our research into their approaches, motives, and patterns of behavior must also grow more complex. 
Cybercrime victims include major corporations, industry-specific news sites, Armed Forces, etc..  These crimes are against very different organizations based on the targetted approach, yet all of these crimes share many common threads